export default function rateConverter(amount: number, rate: number) {
  const convertedAmount = amount * rate;
  return Math.trunc(convertedAmount * 100) / 100;
}
