type Currency = {
  label: string;
  value: string;
  symbol: string;
};

export type SelectedCurrency = {
  currency: string;
  rate: number;
};

export default Currency;
