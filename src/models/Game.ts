type Game = {
  id: string;
  artworkUrl: string;
  name: string;
  rating: number;
  tags: Array<string>;
  releaseDate: string;
  price: number;
};

export type GameCart = {
  nums: number;
  game: Game;
};

export type GamesCart = {
  [key: string]: GameCart;
};

export default Game;
