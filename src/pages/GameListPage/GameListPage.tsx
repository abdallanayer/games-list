import React, { FC, memo, useEffect, useState } from "react";
import Layout from "../../components/Layout/Layout";
import GameDetails from "components/GameDetails/GameDetails";
import Game from "models/Game";
import GameRating from "./components/GameRating/GameRating";
import GameTags from "./components/GameTags/GameTags";
import Button from "components/Button/Button";
import { ReactComponent as Check } from "assets/icons/check.svg";
import { ReactComponent as Plus } from "assets/icons/plus.svg";
import { getGames } from "services/Game";

const GameListPage = memo(() => {
  const [games, setGames] = useState([]);
  useEffect(() => {
    getGames().then((games) => {
      setGames(games);
    });
  }, []);
  return (
    <Layout title="Games">
      {games &&
        games.map((game: Game) => (
          <div data-testid="game-details" key={game.id}>
            <GameDetails
              game={game}
              addButton={(addGameToCart: any) => (
                <Button
                  onClick={addGameToCart}
                  icon={<Check />}
                  color="primary"
                  variant="primary"
                >
                  ADD TO BASKET
                </Button>
              )}
              removeButton={(removeGameFromCart: any) => (
                <Button
                  onClick={removeGameFromCart}
                  icon={<Plus />}
                  color="secondary"
                  variant="secondary"
                >
                  ADDED
                </Button>
              )}
            >
              <GameRating rating={game.rating} />
              <GameTags tags={game.tags} />
            </GameDetails>
          </div>
        ))}
    </Layout>
  );
});

export default GameListPage;
