import React, { FC, memo } from "react";
import { ReactComponent as Star } from "assets/icons/star.svg";
import "./styles.css";
import Label from "components/Label/Label";

const GameRating: FC<{
  rating: number;
}> = ({ rating }) => {
  function getActiveRatingClass(index: number) {
    return rating !== 0 && index + 1 <= rating ? "GameRating-Star-active" : "";
  }
  return (
    <div className="GameRating-container">
      <Label color="secondary">Rating</Label>
      <div>
        {[...Array(5)].map((x, i) => (
          <Star
            key={i}
            className={`GameRating-Star ${getActiveRatingClass(i)}`}
          />
        ))}
      </div>
    </div>
  );
};

export default memo(GameRating);
