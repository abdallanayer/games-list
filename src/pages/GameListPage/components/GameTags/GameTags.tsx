import React, { FC, memo } from "react";
import Label from "components/Label/Label";
import "./styles.css";

const GameTags: FC<{
  tags: Array<string>;
}> = ({ tags }) => {
  return (
    <div className="GameTags-container">
      <Label color="secondary">Tags</Label>
      <div className="GameTags-list">
        {tags.map((tag) => (
          <div className="GameTags-tag" key={tag}>
            {tag}
          </div>
        ))}
      </div>
    </div>
  );
};

export default memo(GameTags);
