import { render, screen, act, fireEvent } from "@testing-library/react";
import GameListPage from "./GameListPage";
import React from "react";
import { games } from "mock/games";
import { CartProvider } from "context/Cart";

let fetchSpy: any;

describe("GameListPage", () => {
  it("Should work", () => {
    const { getByText } = render(<GameListPage />);
    expect(getByText("Games")).toBeInTheDocument();
  });

  describe("GameListPage async", () => {
    beforeEach(() => {
      fetchSpy = jest.spyOn(window, "fetch");
    });
    afterEach(() => {
      fetchSpy.mockReset();
      fetchSpy.mockRestore();
    });
    it("Should call games API", () => {
      render(<GameListPage />);
      expect(window.fetch).toHaveBeenCalledWith("/api/games");
      expect(window.fetch).toHaveBeenCalledTimes(1);
    });
    it("Should render all games returned", async () => {
      fetchSpy.mockResolvedValueOnce({
        json: async () => ({ games }),
      });
      act(() => {
        render(<GameListPage />);
      });
      const gamesRows = await screen.findAllByTestId("game-details");
      expect(gamesRows.length).toBe(games.length);
    });
    it("Should add games to cart", async () => {
      fetchSpy.mockResolvedValueOnce({
        json: async () => ({ games }),
      });
      act(() => {
        render(
          <CartProvider>
            <GameListPage />
          </CartProvider>
        );
      });
      const gamesRows = await screen.findAllByText("ADD TO BASKET");
      await fireEvent.click(gamesRows[0]);
      await fireEvent.click(gamesRows[1]);
      const gamesRowsAdded = await screen.findAllByText("ADDED");
      expect(gamesRowsAdded.length).toBe(2);
      const cartItems = await screen.findByTestId("cart-items");
      expect(cartItems.innerHTML).toEqual("2");
    });
    it("Should update games prices", async () => {
      fetchSpy.mockResolvedValueOnce({
        json: async () => ({ games }),
      });
      act(() => {
        render(
          <CartProvider>
            <GameListPage />
          </CartProvider>
        );
      });
      fetchSpy.mockResolvedValueOnce({
        json: async () => ({ EUR: 0.8738967054 }),
      });
      fireEvent.change(screen.getByTestId("currency-select"), {
        target: { value: "EUR" },
      });

      const gamesPrices = await screen.findAllByTestId("game-price");
      const newPrice = gamesPrices[1].firstChild?.textContent;
      expect(gamesPrices[1].innerHTML).toContain("€");
      expect(newPrice).toEqual("4.1");
    });
  });
});
