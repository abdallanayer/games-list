import React, { memo, FC } from "react";
import "./styles.css";

const OrderDetails: FC<{
  currencySymbol: string;
  orderValues: { price: number; totalItems: number };
}> = ({ currencySymbol, orderValues }) => {
  return (
    <div className="">
      <div className="OrderDetails-key-value">
        <h3>Order Value</h3>
        <h3>
          {currencySymbol}
          {orderValues.price}
        </h3>
      </div>
      <div className="OrderDetails-key-value">
        <h3>Total items</h3>
        <h3>{orderValues.totalItems}</h3>
      </div>
    </div>
  );
};

export default memo(OrderDetails);
