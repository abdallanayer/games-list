import React, { memo } from "react";
import Layout from "../../components/Layout/Layout";
import Button from "../../components/Button/Button";
import { useHistory } from "react-router-dom";
import "./styles.css";
import { CartContext } from "context/Cart";
import { GameCart } from "models/Game";
import GameDetails from "components/GameDetails/GameDetails";
import { ReactComponent as Trash } from "assets/icons/trash.svg";
import OrderDetails from "./components/OrderDetails/OrderDetails";
import { getOrderValues } from "services/Cart";
import Currencies from "constants/Currencies";

const CheckoutPage = memo(() => {
  const history = useHistory();
  const { games, selectedCurrency } = React.useContext(CartContext);

  const orderValues = getOrderValues(games, selectedCurrency);
  const gamesCart = Object.values(games);
  return (
    <Layout
      title="Checkout"
      backButton={{
        text: "Go back to overview page",
        onClick: () => history.push("/list"),
      }}
    >
      <div className="CheckoutPage-container">
        <div className="CheckoutPage-GameList-container">
          {games &&
            gamesCart.map((gameCart: GameCart) => (
              <GameDetails
                key={gameCart.game.id}
                game={gameCart.game}
                removeButton={(removeGameFromCart: any) => (
                  <Button
                    onClick={removeGameFromCart}
                    icon={<Trash />}
                    color="primary"
                    variant="primary"
                  >
                    REMOVE
                  </Button>
                )}
              />
            ))}
        </div>

        <div className="CheckoutPage-Overview-container">
          <OrderDetails
            currencySymbol={Currencies[selectedCurrency.currency].symbol}
            orderValues={orderValues}
          />
          <hr className="CheckoutPage-Divider" />
          <Button
            variant="link"
            color="secondary"
            fullWidth
            onClick={() => history.push("/")}
          >
            Back to overview
          </Button>
        </div>
      </div>
    </Layout>
  );
});

export default CheckoutPage;
