import React, { memo, FC, ReactNode } from "react";
import "./styles.css";

const Button: FC<{
  onClick: () => void;
  children?: any;
  variant: "link" | "secondary" | "primary" | "icon";
  color: "secondary" | "primary" | "link" | "icon";
  fullWidth?: boolean;
  icon?: ReactNode;
}> = ({ children, onClick, variant, color, icon, fullWidth }) => {
  const classNames = ["Button-base"];

  classNames.push(
    `Button-variant${variant.charAt(0).toUpperCase()}${variant.slice(1)}`
  );
  classNames.push(
    `Button-color${color.charAt(0).toUpperCase()}${color.slice(1)}`
  );

  if (!!fullWidth) {
    classNames.push("Button-fullWidth");
  }

  return (
    <button className={classNames.join(" ")} onClick={onClick}>
      {!!icon && <div className="Button-icon">{icon}</div>}

      {children}
    </button>
  );
};

export default memo(Button);
