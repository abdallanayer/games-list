import React, { memo, FC, ReactNode } from "react";
import "./styles.css";

const Label: FC<{
  children: ReactNode;
  color: "secondary" | "primary";
}> = ({ children, color }) => {
  const classNames = ["Label-base"];

  if (color === "secondary") {
    classNames.push("Label-colorSecondary");
  } else {
    classNames.push("Label-colorPrimary");
  }

  return <label className={classNames.join(" ")}>{children}</label>;
};

export default memo(Label);
