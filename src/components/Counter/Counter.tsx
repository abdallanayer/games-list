import React, { FC, memo } from "react";
import Button from "../Button/Button";
import { ReactComponent as Add } from "assets/icons/add.svg";
import { ReactComponent as Subtract } from "assets/icons/subtract.svg";
import "./styles.css";

const Counter: FC<{
  onChange: any;
  count: number;
}> = ({ count, onChange }) => {
  return (
    <div className="Counter-container">
      <Button
        variant="icon"
        color="icon"
        icon={<Subtract />}
        onClick={() => {
          onChange(count - 1);
        }}
      />
      <div>{count}</div>
      <Button
        variant="icon"
        color="icon"
        icon={<Add />}
        onClick={() => {
          onChange(count + 1);
        }}
      />
    </div>
  );
};

export default memo(Counter);
