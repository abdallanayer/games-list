import React, { memo, FC } from "react";
import Select from "./../Select/Select";
import Currencies from "constants/Currencies";
import "./styles.css";

const CurrencySelect: FC<{
  currency: string;
  onChange: (value: string) => void;
}> = ({ currency, onChange }) => {
  function handleOnChangeCurrency(event: any) {
    onChange(event.target.value);
  }

  return (
    <Select
      testId="currency-select"
      value={currency}
      onChange={handleOnChangeCurrency}
      options={Object.values(Currencies)}
    />
  );
};

export default memo(CurrencySelect);
