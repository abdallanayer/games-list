import React, { FC, memo } from "react";
import "./styles.css";

const Select: FC<{
  value: any;
  onChange: any;
  testId: string;
  options: {
    label: string;
    value: any;
  }[];
}> = ({ value, onChange, options, testId }) => {
  return (
    <select
      data-testid={testId}
      className="Select-container"
      value={value}
      onChange={onChange}
    >
      {options.map((option, index) => (
        <option key={option.value + index} value={option.value}>
          {option.label}
        </option>
      ))}
    </select>
  );
};

export default memo(Select);
