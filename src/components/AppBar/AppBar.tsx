import CurrencySelect from "./../CurrencySelect/CurrencySelect";
import React, { memo, FC, useContext } from "react";
import Button from "./../Button/Button";
import { useHistory } from "react-router-dom";
import { ReactComponent as Cart } from "./../../assets/icons/cart.svg";
import { ReactComponent as ArrowBack } from "./../../assets/icons/arrow-back.svg";
import { CartContext } from "context/Cart";
import "./styles.css";
import { getCurrencyRate } from "services/Currency";

export type AppBarProps = {
  title: string;
  backButton?: {
    onClick: () => void;
    text: string;
  };
};

const AppBar: FC<AppBarProps> = memo(({ title, backButton }) => {
  const { selectedCurrency, updateCurrency, games } = useContext(CartContext);
  const history = useHistory();

  async function handleOnChangeCurrency(updatedCurrency: string) {
    const rate = await getCurrencyRate(updatedCurrency);
    updateCurrency({
      currency: updatedCurrency,
      rate: rate[updatedCurrency],
    });
  }

  return (
    <div className="AppBar-container">
      <div className="AppBar-Title-contianer">
        <div className="AppBar-Title-label">{title}</div>

        {!!backButton && (
          <div className="AppBar-BackButton-label" onClick={backButton.onClick}>
            <ArrowBack className="AppBar-BackButton-icon" />

            {backButton.text}
          </div>
        )}
      </div>

      <div className="AppBar-Actions-container">
        <div className="AppBar-Actions-Item-container">
          <Button
            variant="link"
            color="link"
            icon={<Cart />}
            onClick={() => history.push("/checkout")}
          >
            <span className="AppBar-Cart-counter" data-testid="cart-items">
              {Object.keys(games).length}
            </span>
            CHECKOUT
          </Button>
        </div>

        <div className="AppBar-Actions-Item-container">
          <CurrencySelect
            currency={selectedCurrency.currency}
            onChange={handleOnChangeCurrency}
          />
        </div>
      </div>
    </div>
  );
});

export default memo(AppBar);
