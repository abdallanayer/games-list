import React, { FC, memo, ReactNode, useContext, useState } from "react";
import Counter from "../Counter/Counter";
import Game from "../../models/Game";
import GameAvatar from "components/GameAvatar/GameAvatar";
import Currencies from "constants/Currencies";
import "./styles.css";
import { CartContext } from "context/Cart";
import Label from "components/Label/Label";
import rateConverter from "utils/RateConverter";

const GameDetails: FC<{
  game: Game;
  addButton?: any;
  removeButton?: any;
  children?: ReactNode;
}> = ({ game, children, addButton, removeButton }) => {
  const {
    games,
    removeGame,
    addGame,
    updateGame,
    selectedCurrency,
  } = useContext(CartContext);
  const gameNumsInit = games[game.id]?.nums || 1;
  const [gameNums, setGameNums] = useState(gameNumsInit);
  const isGameAddedToCart = games[game.id];
  function updateQuantity(updatedQuantity: number) {
    const nums = updatedQuantity >= 1 ? updatedQuantity : 1;
    updateGame({ nums, game });
    setGameNums(nums);
  }
  function toggleGame() {
    if (!isGameAddedToCart) {
      addGame({ nums: gameNums, game });
    } else {
      removeGame(game.id);
    }
  }
  return (
    <div className="GameDetails-container">
      <div className="GameDetails-Avatar-container">
        <GameAvatar game={game} />
      </div>
      {children}
      <div className="GameDetails-Quantity-container">
        <Label color="secondary">Quantity</Label>
        <Counter count={gameNums} onChange={updateQuantity} />
      </div>
      <div data-testid="game-price" className="GameDetails-price">
        {rateConverter(game.price, selectedCurrency.rate)}
        {Currencies[selectedCurrency.currency].symbol}
      </div>
      <div className="GameDetails-action">
        {!isGameAddedToCart && addButton && addButton(toggleGame)}
        {isGameAddedToCart && removeButton && removeButton(toggleGame)}
      </div>
    </div>
  );
};

export default memo(GameDetails);
