import React, { FC, memo } from "react";
import "./styles.css";
import Label from "components/Label/Label";

const GameAvatar: FC<{
  game: any;
}> = ({ game }) => {
  return (
    <div className="GameAvatar-container">
      <img className="GameAvatar-avatar" src={game.artworkUrl} />
      <div>
        <Label color="secondary">Released - {game.releaseDate}</Label>
        <h4 className="GameAvatar-name">{game.name}</h4>
      </div>
    </div>
  );
};

export default memo(GameAvatar);
