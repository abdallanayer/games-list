export async function getCurrencyRate(currency: string) {
  const response = await fetch(`/api/rates/${currency}`).then();
  const rate = await response.json();
  return rate;
}
