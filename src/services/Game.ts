export function getGames() {
  return fetch(`/api/games`)
    .then((response) => response.json())
    .then((response) => {
      return response.games;
    });
}
