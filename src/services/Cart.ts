import { SelectedCurrency } from "models/Currency";
import rateConverter from "utils/RateConverter";
import { GamesCart } from "../models/Game";

export function getOrderValues(games: GamesCart, currency: SelectedCurrency) {
  const orderDetails = Object.values(games).reduce(
    (accum, gameCart) => {
      accum.totalItems += gameCart.nums;
      accum.price += gameCart.nums * gameCart.game.price;
      return accum;
    },
    { totalItems: 0, price: 0 }
  );

  orderDetails.price = rateConverter(orderDetails.price, currency.rate);
  return orderDetails;
}
