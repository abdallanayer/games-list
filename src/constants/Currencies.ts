import Currency from "models/Currency";

type CurrencyConst = {
  [key: string]: Currency;
};

const Currencies: CurrencyConst = {
  USD: {
    label: "USD ($)",
    symbol: "$",
    value: "USD",
  },
  EUR: {
    label: "EUR (€)",
    symbol: "€",
    value: "EUR",
  },
  GBP: {
    label: "GBP (£)",
    symbol: "£",
    value: "GBP",
  },
};

export default Currencies;
