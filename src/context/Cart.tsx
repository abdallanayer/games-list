import React, { createContext, useReducer, ReactNode } from "react";
import Game, { GameCart, GamesCart } from "../models/Game";
import { SelectedCurrency } from "../models/Currency";

type CartState = {
  games: GamesCart;
  selectedCurrency: SelectedCurrency;
};

const initialState: CartState = {
  games: {},
  selectedCurrency: {
    currency: "USD",
    rate: 1,
  },
};

const actions = {
  UPDATE_CURRENCY: "UPDATE_CURRENCY",
  ADD_GAME: "ADD_GAME",
  REMOVE_GAME: "REMOVE_GAME",
  UPDATE_GAME: "UPDATE_GAME",
};

function addGame(games: GamesCart, addedGame: GameCart) {
  const updatedGames = { ...games };
  updatedGames[addedGame.game.id] = addedGame;
  return updatedGames;
}

function removeGame(games: GamesCart, gameId: string) {
  const updatedGames = { ...games };
  delete updatedGames[gameId];
  return updatedGames;
}

function updateGame(games: GamesCart, updatedGame: GameCart) {
  if (!games[updatedGame.game.id]) return games;
  return addGame(games, updatedGame);
}

function reducer(state: any, action: { type: string; value: any }) {
  switch (action.type) {
    case actions.UPDATE_CURRENCY:
      return { ...state, selectedCurrency: action.value };
    case actions.ADD_GAME:
      return { ...state, games: addGame(state.games, action.value) };
    case actions.REMOVE_GAME:
      return { ...state, games: removeGame(state.games, action.value) };
    case actions.UPDATE_GAME:
      return { ...state, games: updateGame(state.games, action.value) };
    default:
      return state;
  }
}

export const CartContext = createContext({
  ...initialState,
  updateCurrency: (value: SelectedCurrency) => {},
  addGame: (value: GameCart) => {},
  removeGame: (value: string) => {},
  updateGame: (value: GameCart) => {},
});

export function CartProvider({ children }: { children: ReactNode }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  const value = {
    ...state,
    updateCurrency: (value: SelectedCurrency) => {
      dispatch({ type: actions.UPDATE_CURRENCY, value });
    },
    addGame: (value: GameCart) => {
      dispatch({ type: actions.ADD_GAME, value });
    },
    removeGame: (value: string) => {
      dispatch({ type: actions.REMOVE_GAME, value });
    },
    updateGame: (value: GameCart) => {
      dispatch({ type: actions.UPDATE_GAME, value });
    },
  };

  return <CartContext.Provider value={value}>{children}</CartContext.Provider>;
}
